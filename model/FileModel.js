const mongoose = require('mongoose');

const FileSchemer = mongoose.Schema({
    fileName :{
        type: String,
        require: true,
    },

    fileType :{
        type: String,
        require: true,
    },

    img	 :{
        type: String,
        require: true,
    },

    thumbnail	 :{
        type: String,
        require: true,
    },

})

module.exports = mongoose.model('Images', FileSchemer)