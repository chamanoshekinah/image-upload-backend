const express = require('express');
const router = express.Router();
const FileController = require('../controller/FileController');
const FController = new FileController()

router.post('/fileByType', function (req, res) {
   FController.getAllFile(req, res);
});


router.post('/', function (req, res) {
    FController.createFile(req, res)
})


 module.exports = router;