const express = require('express');
const bodyparser = require('body-parser');
const FileRoutes = require ('./routes/FileRoutes');
const mongoose = require('mongoose')
var cors = require('cors')
require('dotenv').config()
const fileUpload = require('express-fileupload');

var app = express();
app.use(cors())
// enable files upload
app.use(fileUpload({
   createParentPath: true
}));

app.use(bodyparser.urlencoded({ extended: true}));
app.use(bodyparser.json());
app.use(bodyparser.raw());
mongoose.connect(process.env.DB_CONNECTION, {useNewUrlParser: true}, ()=> {
   console.info('ohh, mongo db is connected');
})


app.use('/file', FileRoutes);
app.use('/uploads', express.static('uploads'));

var server = app.listen(process.env.PORT,"127.0.0.1", function () {
   var host = server.address().address
   var port = server.address().port
   console.log(server.address())
   console.log("Example app listening at "+host+":"+port)
})