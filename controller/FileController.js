const FileModel = require('../model/FileModel')
const sharp = require('sharp');

class FileController {

    async createFile(req, res) {

        // console.log(req.files.file.name)
        // console.log(req.files.file.mimetype)
        // console.log(req.files.file.size)
        // console.log(req.files.file)
        //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
        let image = req.files.file;
        //Use the mv() method to place the file in upload directory (i.e. "uploads")

        let tempImage = 'uploads/images/' + Math.floor(Date.now() / 1000) + '_' + image.name
        let thumbnail = 'uploads/thumbnail/ ' + Math.floor(Date.now() / 1000) + '_' + image.name
        image.mv(tempImage, (error) => {

            if (error) {
                return console.error("Error  moving file", error)
            } else {
                console.log("success! -- image uploaded")


                sharp(tempImage).resize({ height: 150, width: 150 }).toFile(thumbnail)
                    .then(function (newFileInfo) {
                        console.log("Success image resized to 150 x 150 ", thumbnail);
                    })
                    .catch(function (err) {
                        console.log("Error Failed to resize 1560x1600", err);
                    });


            }

        });

        //saving to DB
        const data = new FileModel({
            fileName: image.name,
            fileType: req.files.file.mimetype,
            img: tempImage,
            thumbnail: thumbnail
        });

        try {
            const saveData = await data.save();
            const data_res = {
                status: 200,
                message: "Successfully Uploaded",
                data: saveData
            }
            res.send(data_res);

        } catch (err) {
            const error_res = {
                status: false,
                message: "failed to save image to db "
            }
            res.send(error_res)
        }
    }

    async getAllFile(req, res) {
        let ip = req.connection.localAddress + ":" + req.connection.localPort;
        console.info(
            ip
        );
        try {
            const type = req.body.type;
            const data = type==='all' ? await FileModel.find() : await FileModel.find({fileType: type});
            const data_res = {
                status: true,
                message: "",
                ip: ip,
                data: data
            }
            res.send(data_res)
        } catch (err) {
            const error_res = {
                status: false,
                message: "Failed to get all image Data from DB: " + err,
            }
            res.send(error_res)
        }
    }


}

module.exports = FileController